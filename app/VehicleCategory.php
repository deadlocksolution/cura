<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VehicleCategory extends Model {
	public $timestamps = false;
	protected $primaryKey = 'VehicleCategoryId';
    protected $table = 'tbl_vehiclecategory';
}

