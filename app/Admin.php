<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model {
	public $timestamps = false;
	protected $primaryKey = 'AdminId';
    protected $table = 'tbl_admin';
}

