<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    public $timestamps    = false;
    protected $primaryKey = 'VehicleId';
    protected $table      = 'tbl_vehicles';

    public function brand_data()
    {
        return $this->hasOne(Brand::class, 'BrandId', 'BrandId');
    }

    public function line_data()
    {
        return $this->hasOne(Line::class, 'LineId', 'LineId');
    }

    public function currency_data()
    {
        return $this->hasOne(Currency::class, 'CurrencyId', 'CurrencyId');
    }

    public function favourite_data()
    {
        return $this->HasMany(Favourite::class, 'VehicleId');
    }

}
