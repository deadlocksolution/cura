<?php
namespace App\Http\Controllers;

use App\City;
use App\Country;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class CountryController extends Controller
{

    public function Index()
    {

        $countryArr = Country::select()->get();
        $dataArr = City::with('CountryData')->where('IsActive',1)->get();

        return view('admin.Country.index', compact('dataArr','countryArr'));

    }

    public function Create()
    {
        $countryArr = Country::select()->get();
        return view('admin.Country.add',compact('countryArr'));
    }

    public function Store(Request $request)
    {

        $country                    = new Country();
        $country->CountryName       = $request->CountryName;
        $country->save();

        return view('admin.Country.index');

    }

    public function Edit($id)
    {
        $CityData = City::find($id);
        return view('admin.Country.edit', compact('CityData'));
    }

    public function StoreCity(Request $request){

        $city                 = new City();
        $city->CityName       = $request->CityName;
        $city->CountryId      = $request->CountryId;
        $city->Latitude       = $request->Latitude;
        $city->Longitude      = $request->Longitude;
        $city->Currency       = $request->Currency;
        $city->save();

        return view('admin.Country.index');
    }

    public function GetCity($id)
    {
        $CityData = City::where('CountryId',$id)->where('IsActive',0)->get();
        return json_encode($CityData);

    }

    public function Update(Request $request)
    {

        $id                      = $request->id;
        $city                    = City::find($id);
        $city->Latitude          = $request->Latitude;
        $city->Longitude         = $request->Longitude;
        $result                  = $city->save();

        return Redirect::to('country');    

    }

    public function ActivateCity(Request $request)
    {

        City::where('CountryId', $request->CountryId)
            ->where('CityId', $request->CityId)
            ->update([
                'IsActive' => 1
            ]);

        return Redirect::to('country');  
        
    }

    public function Destroy($id)
    {

        Brand::where('BrandId', $id)->delete();
        echo 'success';
        exit();
    }

}
