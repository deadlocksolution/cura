<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Notification;
use App\Notificationsolog;
use App\SentPushLog;
use App\User;
use App\PushUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Validator;
use DB;

class NotificationController extends Controller
{

    public function Index()
    {
        $UserData = User::all();
        return view('admin.Notification.push', compact('UserData'));
    }

    public function Store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'NotificationText' => 'required',
            ]
        );


        // echo "<pre>";
        // print_r($request->all());
        // print_r($request->UserId);
        // exit();

        $notificationType = '1';
        $message          = $request->NotificationText;

        $dataArr = User::where('tbl_Users.Status', '1')
                            ->Join('tbl_pushuser as pu', 'pu.UserId', '=', 'tbl_users.UserId')
                            // ->where('tbl_users.UserType', '!=' , 2)
                            // ->where('pu.DeviceToken' , 'eP_LFwSxpus:APA91bGJNHyfIv49ob8N9u6EezqAxy2rUhug5OKScw83HHuYVKV0SNe_3tU76JLQB6X85S3ziXKArbJMKRVpw-oTsetJTkU_BlV_NcWj9DJIZLI2H25GADausnnkFzhZvqi6A35-o_-c')
                            ->get();

        $resArr = array();
        if (count($request->UserId) > 0) {

            $notification = new Notification();
            $addPush = $notification->add_pushNotification('', $message, $notificationType);

            foreach ($request->UserId as $ke => $va) {

                $sendPush = $this->push_notification($va['DeviceToken'], $message, $notificationType, $va['DeviceType']);

                $addSentPush = $notification->add_sentPushLog($va['UserId'], $message, $addPush, $notificationType,'');

            }

        }

        flash()->success('Send Push Successfully');
        return Redirect::to('notification');
    }

    public function Create()
    {
        return view('admin.Notification.add');
    }

    public function Add(Request $request)
    {
        $notification                   = new Notification();
        $notification->Latitude         = $request->Latitude;
        $notification->Longitude        = $request->Longitude;
        $notification->Range            = $request->Range;
        $notification->NotificationText = $request->NotificationText;
        $notification->NotificationType = '2';
        $result                         = $notification->save();

        if ($result > 0) {
            flash()->success('Push By Location Inserted Successfully');
            return Redirect::to('push-location');
        } else {
            flash()->warning('Error on Create push by location');
            return Redirect::back();
        }

    }

    public function Edit($id)
    {
        // $NotificationData = Notification::find($id);
        // return view('admin.Notification.edit', compact('NotificationData'));
    }

    public function Update(Request $request)
    {

        // $id                             = $request->id;
        // $notification                   = Notification::find($id);
        // $notification->NotificationText = $request->NotificationText;
        // $notification->Latitude         = $request->Latitude;
        // $notification->Longitude        = $request->Longitude;
        // $notification->Range            = $request->Range;

        // $result = $notification->save();

        // flash()->success('Push By Location Updated Successfully');
        // return Redirect::to('push-location');

    }

    public function Destroy($id)
    {

        // Notification::where('NotificationId', $id)->delete();
        // echo 'success';
        // exit();
    }

    public function test(){
    	$this->push_notification('eWZ4cypTqfE:APA91bEyCzreQxbGCreoFIFqgsJHV80DpV_eWy_ThfPvjA0BBOa10zrht_yyKvWiCS02RULCNg8-fDOs5OS7xK9SYXULzycTEtofPUOF-UyFJIV8lawAQSNI3_jIthSNg7LlqUmOaR4k', 'Test android push', '1', 'android');

        $this->push_notification('dB-RH4r_owg:APA91bGSbcdGLg2zg_4slEAmlottgy3087KCCJpg7SPa7pl0UTG6AlxWJwdhe6wmMHQ1Ap4_piaUdVqXHvFY3uuWZSREFiHspaDvBznK0oBPTbH7bsT3KPoUVAaYIfLwYAy8AlsAbWK4', 'Test ios push', '1', 'ios');
    }

    

    public function push_notification($DeviceToken, $message, $type, $device_os)
    {
        //API URL of FCM
        $url = 'https://fcm.googleapis.com/fcm/send';

        $DeviceToken = array($DeviceToken);

        $api_key = 'AAAAAdH7P6E:APA91bHC3IISlxHxcBOJDx-VluTkBwx7HMSF-u5nqBkY1tU5cG9RLRQe3497kJsjorsg_rgkjscRakClaTfnWTnWjv9aqG1kR9qpo1PMrYcbsxbQ2znMc8ngsBMybUTnjihaFGZYR24v';

        if ($device_os == "Ios" || $device_os == "ios") {
            $fields = array(
                'registration_ids' => $DeviceToken,
                'priority'         => 10,
                'notification'     => array('title' => 'CURA', 'body' => $message, 'sound' => 'Default'),
                'data'             => array('message' => $message, 'type' => $type),
            );
        }

        if ($device_os == "Android" || $device_os == "android") {
            $fields = array(
                'registration_ids' => $DeviceToken,
                'data'             => array("message" => $message, 'type' => $type,'title' => 'CURA'),
            );
        }

        //header includes Content type and api key
        $headers = array(
            'Content-Type:application/json',
            'Authorization:key=' . $api_key,
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        if ($result === false) {
            die('FCM Send Error: ' . curl_error($ch));
        }
        curl_close($ch);
        // $this->load->helper('file');
        $log = "---------------------------------------------------------------------------\n\n";
        $log .= "LogAt: " . gmdate('Y-m-d H:i:s') . "\n\n";
        $log .= "Type: " . $type . "\n\n";
        $log .= "Message: " . $message . "\n\n";
        $log .= "Device Os: " . $device_os . "\n\n";
        // $log .="Data: [Order->".$order_id."]--[restaurant_name->".$restaurant_name."]--[pickup_address->".$pickup_address."]--[delivery_address->".$delivery_address."]--[order_request_id->".$order_request_id."]--[customer_name->".$customer_name."]\n\n";
        // $log .="ids: ".json_encode($device_ids)."\n\n";
        $log .= "Result: " . $result . "\n\n";
        $log .= "---------------------------------------------------------------------------\n\n";
        $file_path = "LIVE_PUSH_DEBUG1.txt";
        if (file_exists($file_path)) {
            Storage::put($file_path, $log);
        } else {
            Storage::put($file_path, $log);
        }

        // print_r($result);exit;
        // return $result;
    }
}
