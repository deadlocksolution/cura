<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\User;
use App\Admin;
use App\Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Session;

class AdminController extends Controller {

	public function Login(Request $request) {
		// 		if (Auth::check()) {
		//     return Redirect::to('/admin/dashboard');
		// }
		return view('admin/auth/login');

	}

	public function PostLogin(Request $request) {

		$email = $request->input('email');
		$password = md5($request->input('password'));

		$data = Admin::select()
				->where(['Email' => $email])
				->where(['Password' => $password])
				->first();

		if (empty($data)) {
			return view('admin/auth/login', ["msg" => 'email and password are wrong.']);
		}
		
		Session::put('AdminId', $data->AdminId);
		Session::put('Email', $data->Email);
		Session::put('Type', $data->UserType);

		return Redirect::to('user');	
		
	}

	public function Dashboard() {

		return view('admin/dashboard');

	}

	public function Logout() {

		Session::flush();
		// Session::forget('AdminId');
		// Session::forget('Email');

		return Redirect::to('/');

	}

	public function GetHelpText(){
		$helptext = Config::pluck('HelpText')->first();
		return view('admin/helptext', compact('helptext'));
	}

	public function SaveHelpText(Request $request){

		$helptext = Config::find(1);
		$helptext->HelpText = (string)$request->HelpText;
		$helptext->save();

		return response()->json(['success' => '1']);
	}

	public function Index(){
		$dataArr = Admin::select()->get();
        return view('admin.Config.index', compact('dataArr'));
	}

	public function Config(Request $request){

        $admin                      = Admin::find(Session::get('AdminId'));
        $admin->ActiveProductDays   = $request->ActiveProductDays;
        $admin->IsValidateToken     = $request->IsValidateToken;
        $admin->save();

        return 'true';
	}
}