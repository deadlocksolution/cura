<?php
namespace App\Http\Controllers;
use App\VehicleModel;
use App\Brand;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class ModelController extends Controller
{

    public function Index($type)
    {

        $dataArr = VehicleModel::where(['BrandId' => $type])->where('status',1)->get();

        return view('admin.Model.index', compact('dataArr'));

    }

    public function Create($BrandId)
    {
        $brandArr = Brand::where('BrandId',$BrandId)->orderBy("BrandName", "ASC")->get();
        return view('admin.Model.add',compact('brandArr'));
        // return view('admin.Model.add');
    }

    public function Store(Request $request)
    {

        $model                    = new VehicleModel();
        $model->ModelName         = $request->ModelName;
        $model->BrandId           = $request->BrandId;
        $model->save();

        return Redirect::to('model/'.$request->BrandId);    

    }

    public function Edit($id)
    {

        $ModelData = VehicleModel::find($id);

        return view('admin.Model.edit', compact('ModelData'));
    }

    public function Update(Request $request)
    {

        $id                       = $request->id;
        $model                    = VehicleModel::find($id);
        $model->ModelName         = $request->ModelName;
        $result                   = $model->save();
        
        return Redirect::to('model/'.$request->BrandId);  

    }

    public function Destroy($id)
    {

        VehicleModel::where('ModelId', $id)->delete();
        echo 'success';
        exit();
    }

}
