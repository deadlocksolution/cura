<?php
namespace App\Http\Controllers;

use App\Brand;
use App\VehicleModel;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class BrandController extends Controller
{

    public function Index()
    {

        $dataArr = Brand::with('ModelData') 
            ->orderBy("BrandId", "ASC")
            ->get();

        return view('admin.Brand.index', compact('dataArr'));

    }

    public function Create()
    {
        return view('admin.Brand.add');
    }

    public function Store(Request $request)
    {

        $brand                    = new Brand();
        $brand->BrandName         = $request->BrandName;
        $brand->save();

        return Redirect::to('brand');    
    
    }

    public function Edit($id)
    {
        $BrandData = Brand::find($id);
        $modelArr = VehicleModel::orderBy("ModelName", "ASC")->get();
        return view('admin.Brand.edit', compact('BrandData','modelArr'));
    }

    public function Update(Request $request)
    {

        $id                       = $request->id;
        $brand                    = Brand::find($id);
        $brand->BrandName         = $request->BrandName;
        $result = $brand->save();

        return Redirect::to('brand');    
        
    }

    public function Destroy($id)
    {

        Brand::where('BrandId', $id)->delete();
        echo 'success';
        exit();
    }

}
