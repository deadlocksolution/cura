<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Config extends Model {
	public $timestamps = false;
	protected $primaryKey = 'ConfigId';
	protected $table = 'tbl_config';
}
