<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
        $schedule->call('App\Http\Controllers\NotificationController@PostExpirationReminder')->everyMinute();
        $schedule->call('App\Http\Controllers\NotificationController@SendDealOftheWeekReminderToDealer')->everyMinute();
        $schedule->call('App\Http\Controllers\NotificationController@SendDealOftheWeekReminderToUser')->everyMinute();
        $schedule->call('App\Http\Controllers\NotificationController@ResetOffers')->everyMinute();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
