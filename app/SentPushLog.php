<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SentPushLog extends Model {
	public $timestamps = false;
	protected $primaryKey = 'SentPushId';
    protected $table = 'tbl_sentpushlogs';
}

