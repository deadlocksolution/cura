<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model {
	public $timestamps = false;
	protected $primaryKey = 'country_id';
	protected $table = 'tbl_country';

	// public function CountryData()
 //    {
 //        return $this->hasOne(Country::class, 'CountryId', 'CountryId');
 //    }

}
