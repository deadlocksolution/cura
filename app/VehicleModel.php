<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VehicleModel extends Model {
	public $timestamps = false;
	protected $primaryKey = 'ModelId';
    protected $table = 'tbl_model';

}

