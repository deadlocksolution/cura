<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model {
	public $timestamps = false;
	protected $primaryKey = 'BrandId';
    protected $table = 'tbl_brand';

    public function ModelData()
    {
        return $this->hasOne(VehicleModel::class, 'ModelId', 'ModelId');
    }
}

