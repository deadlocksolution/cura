$(document).ready(function() {
    $(".only_numbers").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
});

function changeCountry(current_value) {
    $.ajax({
        type: "GET",
        url: 'get_state',
        data: {
            country_id: current_value
        },
        success: function (data) {
            $('#city').prop('disabled', false).html('').append('<option value="">Select City</option>');
            $('#state').prop('disabled', false).html('').append('<option value="">Select State</option>');
            for (var i = 0; i < data.data.length; i++) {
                $('#state').append('<option value=' + data.data[i].id + '>' + data.data[i].name + '</option>');
            }
        }
    });
}

function changeState(current_value) {
    $.ajax({
        type: "GET",
        url: 'get_city',
        data: {
            state_id: current_value
        },
        success: function (data) {
            $('#city').prop('disabled', false).html('').append('<option value="">Select City</option>');
            for (var i = 0; i < data.data.length; i++) {
                $('#city').append('<option value=' + data.data[i].id + '>' + data.data[i].name + '</option>');
            }
        }
    });
}

function validateSignup(url) {
    
    $("#password_length_error").hide();
    $("#password_mismatch_error").hide();
    $("#phone_already_exist_error").hide();
    
    var password = $("#password").val();
    var cpassword = $("#cpassword").val();
    
    password = $.trim(password);
    cpassword = $.trim(cpassword);
    
    if(password.length < 5) {
        $("#password_length_error").show();
        return false;
    }
    
    if(password != cpassword) {
        $("#password_mismatch_error").show();
        return false;
    }
    
    var formData = $( "form" ).serialize();
    
    $.ajax({
        type: "POST",
        url: url,
        data: formData,
        success: function (data) {

            if($.trim(data) == "user_exist") {
                $("#phone").focus();
                $("#phone_already_exist_error").show();
                return false;
            }
            
            $("#signup_modal_user_id").val($.trim(data));
            
            $("#signup_success_modal").modal({backdrop: 'static', keyboard: false});
            $("#signup_success_modal").modal("show");
        }
    });
    
    return false;
}

function validateUserOtp() {
    var user_id = $("#signup_modal_user_id").val();
    var otp = $("#otp_verify_popup").val();
    
    $("#otp_invalid_error").hide();
    $("#user_not_found_error").hide();
    $("#user_already_verified_error").hide();
    
    if(otp == "") {
        $("#otp_verify_popup").focus();
        return false;
    }
    
    if(user_id != "" && otp != "") {
        $.ajax({
            type: "get",
            url: "validate_user_otp",
            async: false,
            data: {
                user_id : user_id,
                otp : otp,
            },
            success: function (res) {
                if(res == "success") {
                    $("#verify_success").show();
                    
                    setTimeout(function(){ 
                        window.location.href = 'login';
                    }, 3000);
                } else {
                    if (res == "otp_invalid") {
                        $("#otp_invalid_error").show();
                    }
                    if(res == "user_not_found") {
                        $("#user_not_found_error").show();
                    }
                    if(res == "user_already_verified") {
                        $("#user_already_verified_error").show();
                    }
                }
            }
        });
    }   
}

function regenerateUserOtp() {
    var user_id = $("#signup_modal_user_id").val();
    
    $("#regenerate_otp_success").hide();
    $("#user_not_found_error").hide();
    $("#user_already_verified_error").hide();
    
    if(user_id != "") {
        $.ajax({
            type: "get",
            url: "regenerate_user_otp",
            async: false,
            data: {
                user_id : user_id,
            },
            success: function (res) {
                if(res == "success") {
                    $("#regenerate_otp_success").show();
                    
                    setTimeout(function(){ 
                        $("#regenerate_otp_success").hide();
                    }, 3000);
                    
                }
                if(res == "user_not_found") {
                    $("#user_not_found_error").show();
                }
                if(res == "user_already_verified") {
                    $("#user_already_verified_error").show();
                }
            }
        });
    }
}

function deletePopup(path, id) {
    $("#delete_confirm_btn").attr('onclick', 'deleteRecord("' + path + '", ' + id + ')');
    $('#deleteModal').modal('show');
}
function deleteRecord(path, id) {
    $.ajax({
        type: "GET",
        url: path + id,
        success: function (res) {

            $("#record_" + id).hide();
            $("#deleteModal").modal("hide");
            if(path == 'user/delete/'){
                location.reload(true);
            }
        }
    });
}

function vehicleImageModel(VehicleId){
    var url = '../vehicleImages/'+VehicleId;
    $.ajax({
        type: "GET",
        url: url,
        success: function (res) {
            var obj = JSON.parse(res);
            var html = '';                        
            if(obj.length > 0){
                var i=0;
                obj.forEach(function(item) {

                    var url = '../../'+item;
                    imageId= 'image_'+i;
                    html += '<div class="col-md-4" id="'+imageId+'">'+
                                '<img src="'+url+'" class="img">'+
                                '<span class="remove_img_preview" onclick="deleteVehicleImageConfirmationModel1('+VehicleId+','+i+');"></span>'+
                            '</div>';   
                    i++;
                });
            }
            $('#VehicleImageModal .modal-body').html(html);
        }
    });
    $('#VehicleImageModal').modal('show');
}

function checkUserExist() {
    $("#password_length_error").hide();
    $("#password_mismatch_error").hide();
    $("#phone_already_exist_error").hide();
    
    var password = $("#password").val();
    var cpassword = $("#cpassword").val();
    
    password = $.trim(password);
    cpassword = $.trim(cpassword);
    
    if(password.length < 5) {
        $("#password_length_error").show();
        return false;
    }
    
    if(password != cpassword) {
        $("#password_mismatch_error").show();
        return false;
    }
    
    var phone = $("#phone").val();
    
    var is_user_exist = 1;
    $.ajax({
        type: "get",
        url: "validate_user_phone_exist",
        async: false,
        data: {
            phone: phone
        },
        success: function (data) {
            if($.trim(data) == "user_exist") {
                $("#phone_already_exist_error").show();
            } else {
                is_user_exist = 0;
            }
        }
    });
    
    if(is_user_exist == 1) {
        return false;
    } else {
        return true;
    }
}

function validateEditUser() {
    $("#password_length_error").hide();
    $("#password_mismatch_error").hide();
    
    var password = $("#password").val();
    var cpassword = $("#cpassword").val();
    
    password = $.trim(password);
    cpassword = $.trim(cpassword);
    
    if(password != "" && password.length < 5) {
        $("#password_length_error").show();
        return false;
    }
    
    if(password != "" && password != cpassword) {
        $("#password_mismatch_error").show();
        return false;
    }
    
    return true;
}

function addBanners() {
    var cnt = parseInt($("#cnt").val());
    var new_cnt = cnt + 1;
    
    var html = '<div class="banner_div_' + new_cnt + ' pull-left width-100 mb-20">';
            html+= '<div class="col-sm-8">';
                html+= '<input type="file" name="banner_' + new_cnt + '">';
            html+= '</div>';
            html+= '<div class="col-sm-4">';
                html+= '<i class="fa fa-2x fa-minus color-red cursor-pointer" onclick="removeBanners(' + new_cnt + ');"></i>';
            html+= '</div>';
        html+= '</div>';
    
    $("#add_banner_div").append(html);
    
    $("#cnt").val(new_cnt);
}

function removeBanners(current_cnt) {
    $(".banner_div_" + current_cnt).slideUp().remove();
}

function deleteBannerConfirmationModel(banner_id) {
    $("#banner_delete_confirm_btn").attr('onclick', 'deleteExistingBanner(' + banner_id + ')');
    $('#deleteBannerModal').modal('show');
}

function deleteExistingBanner(banner_id) {
    $.ajax({
        type: "GET",
        url: "delete_banner/" + banner_id,
        success: function (res) {
            $("#banner_" + banner_id).hide();
            $("#deleteBannerModal").modal("hide");
        }
    });
}

function addMoreAddress() {
    var cnt = parseInt($("#cnt").val());
    var new_cnt = cnt + 1;
    
    var html = '<div class="row" id="address_box_' + new_cnt + '">';
            html+= '<div class="col-md-12 row">';
                html+= '<div class="col-md-11">';
                    html+= '<input type="text" class="form-control mb-20" name="address[]">';
                html+= '</div>';
                html+= '<div class="col-md-1">';
                    html+= '<i class="fa fa-minus color-red fa-2x cursor-pointer" onclick="removeAddress(' + new_cnt + ');"></i>';
                html+= '</div>';
            html+= '</div>';
        html+= '</div>';
    
    $("#address_div").append(html);
    
    $("#cnt").val(new_cnt);
}

function removeAddress(current_cnt) {
    $("#address_box_" + current_cnt).slideUp().remove();
}

function addProductPriceBlock() {
    var cnt = parseInt($("#price_cnt").val());
    var new_cnt = cnt + 1;
    
    var html = '<div class="col-md-12 row mt-10" id="price_block_' + new_cnt + '">';
            html+= '<div class="form-group">';
                html+= '<div class="col-md-3">';
                    html+= '<input type="text" class="form-control" placeholder="Quantity Range Ex., 1-5" name="price_title_' + new_cnt + '" required="">';
                html+= '</div>';
                html+= '<div class="col-md-3">';
                    html+= '<input type="text" class="form-control" placeholder="Price Ex., 15000" name="price_value_' + new_cnt + '" required="">';
                html+= '</div>';
                html+= '<div class="col-md-3">';
                    html+= '<i class="fa fa-minus color-red fa-3x cursor-pointer" onclick="removeProductPriceBlock(' + new_cnt + ');"></i>';
                html+= '</div>';
            html+= '</div>';
        html+= '</div>';
    
    $("#price_tier").append(html);
    
    $("#price_cnt").val(new_cnt);
}

function removeProductPriceBlock(current_cnt) {
    $("#price_block_" + current_cnt).slideUp().remove();
}

function addProductCategoryBlock() {
    var cnt = parseInt($("#category_cnt").val());
    var new_cnt = cnt + 1;
    
    var html = '<div class="col-md-12 row mt-10" id="category_block_' + new_cnt + '">';
            html+= '<div class="form-group">';
                html+= '<div class="col-md-4">';
                    html+= '<select class="form-control" id="category_' + new_cnt + '" name="category_' + new_cnt + '" required="" onchange="changeProductCategory(this.value, ' + new_cnt + ');">';
                        html+= '<option value="">Select Category</option>';
                        html+= $("#category_html").html();
                    html+= '</select>';
                html+= '</div>';
                html+= '<div class="col-md-4">';
                    html+= '<select class="form-control" id="sub_category_' + new_cnt + '" name="sub_category_' + new_cnt + '">';
                        html+= '<option value="">Select Sub Category</option>';
                    html+= '</select>';
                html+= '</div>';
                html+= '<div class="col-md-3">';
                    html+= '<i class="fa fa-minus color-red fa-3x cursor-pointer" onclick="removeProductCategoryBlock(' + new_cnt + ');"></i>';
                html+= '</div>';
            html+= '</div>';
        html+= '</div>';

    $("#category_tier").append(html);
    
    $("#category_cnt").val(new_cnt);
}

function removeProductCategoryBlock(current_cnt) {
    $("#category_block_" + current_cnt).slideUp().remove();
}

function changeProductCategory(current_value, current_cnt) {
    
    var html = '<option value="">Select Sub Category</option>';
    if(current_value == "") {
        $("#sub_category_" + current_cnt).html('').html(html);
        return false;
    }
    
    $.ajax({
        type: "GET",
        url: 'get_product_sub_category',
        data: {
            category_id: current_value
        },
        success: function (data) {
            $('#sub_category_'+current_cnt).prop('disabled', false).html('').html(html);
            for (var i = 0; i < data.data.length; i++) {
                $('#sub_category_'+current_cnt).append('<option value=' + data.data[i].id + '>' + data.data[i].name + '</option>');
            }
        }
    });   
}

function addVehicleImageBlock() {
    var cnt = parseInt($("#image_cnt").val());
    var new_cnt = cnt + 1;
    
    var html = '<div class="col-md-4 row mt-10" id="image_block_' + new_cnt + '">';
            html+= '<div class="form-group">';
                html+= '<div class="col-md-10">';
                    html+= '<img src="" id="vehicle_image_' + new_cnt + '" style="display: none;" height="200px" width="200px">';
                    html+= '<div class="fileupload btn btn-info btn-anim">';
                        html+= '<i class="fa fa-upload cursor-pointer"></i><span class="btn-text">Upload image</span>';
                        html+= '<input type="file" class="upload" required="" name="image_' + new_cnt + '" id="image_' + new_cnt + '" onchange="uploadVehicleImage(this, ' + new_cnt + ');">';
                    html+= '</div>';
                html+= '</div>';
                html+= '<div class="col-md-2">';
                    html+= '<i class="fa fa-minus color-red fa-3x cursor-pointer" onclick="removeVehicleImageBlock(' + new_cnt + ');"></i>';
                html+= '</div>';
            html+= '</div>';
        html+= '</div>';
    

    $("#image_tier").append(html);
    
    $("#image_cnt").val(new_cnt);
}

function removeVehicleImageBlock(current_cnt) {
    $("#image_block_" + current_cnt).slideUp().remove();
}

function uploadVehicleImage(input, current_cnt) {
    
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $('#vehicle_image_' + current_cnt).attr('src', e.target.result).show();
        }

        reader.readAsDataURL(input.files[0]);
    }
}

function uploadImage(input) {
    
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $('#img').attr('src', e.target.result).show();
        }

        reader.readAsDataURL(input.files[0]);
    }
}

function uploadImage1(input) {
    
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $('#img1').attr('src', e.target.result).show();
        }

        reader.readAsDataURL(input.files[0]);
    }
}

function uploadImage2(input) {
    
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $('#img2').attr('src', e.target.result).show();
        }

        reader.readAsDataURL(input.files[0]);
    }
}



function removeProductAttributeBlock(current_cnt) {
    $("#attribute_block_" + current_cnt).slideUp().remove();
}


function deleteStoreImageConfirmationModel(StoreId,ImageId){
    $("#store_image_delete_confirm_btn").attr('onclick', 'deleteStoreImageModal('+ StoreId +',' + ImageId + ')');
    $('#deleteStoreImageModal').modal('show');
}
function deleteStoreImageModal(StoreId,ImageId) {
    $.ajax({
        type: "GET",
        url: "delete_store_image",
        data: {
            ImageId : ImageId,
            StoreId : StoreId
        },
        success: function (res) {

            $('#image_'+ImageId).remove();
            var total_img_cnt = $('.image_file_arr_cnt').val();
            image_cnt = parseInt(total_img_cnt)-parseInt(1);

            $('.image_file_arr_cnt').val(image_cnt);

            if(image_cnt == 0){
                $('#files').prop('required',true);
            }

            $("#image_" + ImageId).hide();
            $("#deleteStoreImageModal").modal("hide");
        }
    });
}

function deleteDealerImageConfirmationModel(UserId,ImageId){
    $("#dealer_image_delete_confirm_btn").attr('onclick', 'deleteDealerImageModal('+ UserId +',' + ImageId + ')');
    $('#deleteDealerImageModal').modal('show');
}
function deleteDealerImageModal(UserId,ImageId) {
    $.ajax({
        type: "POST",
        url: "delete_dealer_image",
        data: {
            ImageId : ImageId,
            UserId : UserId
        },
        success: function (res) {
            $('#image_'+ImageId).remove();
            var total_img_cnt = $('.image_file_arr_cnt').val();
            image_cnt = parseInt(total_img_cnt)-parseInt(1);
            $('.image_file_arr_cnt').val(image_cnt);

            if(image_cnt == 0){
                $('#files').prop('required',true);
            }

            $("#image_" + ImageId).hide();
            $("#deleteDealerImageModal").modal("hide");
        }
    });
}


function deleteVehicleImageConfirmationModel(VehicleId,ImageId) {
    $("#vehicle_image_delete_confirm_btn").attr('onclick', 'deleteVehicleImageModal('+ VehicleId +',' + ImageId + ')');
    $('#deleteVehicleImageModal').modal('show');
}
function deleteVehicleImageConfirmationModel1(VehicleId,ImageId) {
    $("#vehicle_image_delete_confirm_btn").attr('onclick', 'deleteVehicleImageModalIndex('+ VehicleId +',' + ImageId + ')');
    $('#deleteVehicleImageModal').modal('show');
}


function deleteVehicleImageModal(VehicleId,ImageId) {

    $.ajax({
        type: "GET",
        url: "delete_vehicle_image",
        data: {
            ImageId : ImageId,
            VehicleId : VehicleId
        },
        success: function (res) {
            // $('.existing_img_'+ImageId).remove();
            var total_img_cnt = $('.image_file_arr_cnt').val();
            image_cnt = parseInt(total_img_cnt)-parseInt(1);
            $('.vehicle_image .image_file_arr_cnt').val(image_cnt);
            if(image_cnt == 0){
                $('#files').prop('required',true);
            }

            // $("#exist_image_" + ImageId).hide();
            // $('#image_'+ImageId).hide();
            $('#image_'+ImageId).remove();
            $("#deleteVehicleImageModal").modal("hide"); 
        }
    });
}

function deleteVehicleImageModalIndex(VehicleId,ImageId) {
    $.ajax({
        type: "POST",
        url: "delete_vehicle_image",
        data: {
            ImageId : ImageId,
            VehicleId : VehicleId
        },
        success: function (res) {

            var total_img_cnt = $('.image_file_arr_cnt').val();
            image_cnt = parseInt(total_img_cnt)-parseInt(1);
            $('.vehicle_image .image_file_arr_cnt').val(image_cnt);
            if(image_cnt == 0){
                $('#files').prop('required',true);
            }

            $('#image_'+ImageId).remove();
            $("#deleteVehicleImageModal").modal("hide");

            var imageCount = $('#record_'+VehicleId+' .image_count').html();
            var imageCountRecord = parseInt(imageCount)-parseInt(1);
            if(imageCountRecord > 0){
                $('#record_'+VehicleId+' .image_count').html(imageCountRecord);
            }else{
                $('#record_'+VehicleId+' .bottom-right').hide();
                $('#exist_image_tier .remove_img_preview').hide();
            }
            
        }
    });
}


function product_search(){
    var searchText = $('#product_search').val().toLowerCase();
    $('.product').each(function () {
        var currentLiText = $(this).text().toLowerCase(),
                  showCurrentLi = currentLiText.indexOf(searchText.toLowerCase()) !== -1;
        
        $(this).toggle(showCurrentLi);

    });

}



$('#pdf').change(function (event) {
    var file = URL.createObjectURL(event.target.files[0]);
    $('#element').append('<a href="' + file + '" target="_blank">' + event.target.files[0].name + '</a><br>');
    $("#save_btn").removeAttr("disabled");
});

// MUltiple images

var count=0;
var count1=0;
function handleFileSelect(evt) {
    var $fileUpload = $("input#files[type='file']");
    var image_file_arr_cnt = $(".image_file_arr_cnt").val();
    var count=parseInt(image_file_arr_cnt)+parseInt($fileUpload.get(0).files.length);
    // var count=parseInt($fileUpload.get(0).files.length);
    // var count=parseInt($fileUpload.get(0).files.length);
    $('.image_file_arr_cnt').val(count);
    
    // if (parseInt($fileUpload.get(0).files.length) > 6 || count>5) {
    if (parseInt($fileUpload.get(0).files.length) > 25 || count>20) {
        alert("You can only upload a maximum of 20 files");
        count=count-parseInt($fileUpload.get(0).files.length);
        evt.preventDefault();
        evt.stopPropagation();
        return false;
    }
    var files = evt.target.files;

    for (var i = 0, f; f = files[i]; i++) {
        if (!f.type.match('image.*')) {
            continue;
        }
        var reader = new FileReader();

        reader.onload = (function (theFile) {
            return function (e) {
                count1++;
                // var span = document.createElement('span');
                // span.setAttribute('class', 'new_image');
                // span.innerHTML = ['<img class="thumb" src="', e.target.result, '" title="', escape(theFile.name), '"/><span class="remove_img_preview"></span>'].join('');
                // document.getElementById('list').insertBefore(span, null);
                html = '<li class="ui-state-default ui-sortable-handle new_image" id="image_'+count1+'">'+
                          '<img class="thumb" src="'+e.target.result+'" title="">'+
                          '<span class="remove_img_preview"></span>'+
                        '</li>'
                $('#sortable').append(html);
                console.log("after : "+count1);
                $('#count').val(count);
            };
        })(f);

        reader.readAsDataURL(f);
    }
}

$('#files').change(function(evt){
    handleFileSelect(evt);
});  

// $('#list').on('click', '.new_image .remove_img_preview',function () {
//     $(this).parent('span').remove();
//     $(this).parent('span').splice( 1, 1 );
    
//     count--;
// });
$('#sortable').on('click', '.new_image .remove_img_preview',function () {
    $(this).parent('li').remove();
    $(this).parent('li').splice( 1, 1 );

    var count=0;
    var count = $(".image_file_arr_cnt").val();

    count--;

    $('.image_file_arr_cnt').val(count);
});

function checkUniqueEmail(email){
    var email = email.value;

    var currentUrl = $(location).attr('href');

    if(currentUrl.indexOf('store/user') != -1){
        var url= "../../user/CheckUniqueEmail";
    }else{
        var url= "../user/CheckUniqueEmail";
    }

    $.ajax({
        type: "POST",
        url: url,
        data: {
            'email' : email
        },
        success: function (response) {

            if(response == 'true'){
                // console.log('unique email');
                $("#email").css('border-color','rgb(204, 204, 204)');
                $('.email-error').hide();
            }else{
                // console.log('error');
                $("#email").focus();
                $("#email").css('border-color','red');
                $('.email-error').show();
            }

        }
    });
}

function validateAddEditVehicle() {
            
    var vehiclecategory = $("#vehiclecategory").val();
    var brands = $("#brands").val();
    var lineas = $("#lineas").val();
    var model = $("#model").val();
    var state = $("#state").val();

    var css_error = ({
        'border-color': 'red'
    });
    var css_original = ({
        'border-color': 'rgb(204, 204, 204)'
    });

    $("#vehiclecategory,#brands,#lineas,#model,#state").css(css_original);

    var file = $("#image").val();
    var exts = ['jpg', 'jpeg', 'png', 'gif', 'bmp'];

    var imageValidate = "1";
    if (file) {
        var get_ext = file.split('.');
        get_ext = get_ext.reverse();
        if ($.inArray(get_ext[0].toLowerCase(), exts) > -1) {
            imageValidate = "1";
        } else {
            $("#img_error").slideDown("slow");
            imageValidate = "0";
            return false;
        }
    }

    if (vehiclecategory == "") {
        $("#vehiclecategory").css(css_error).focus();
        return false;
    }else if (brands == "") {
        $("#brands").css(css_error).focus();
        return false;
    } else if(lineas == "") {
        $("#lineas").css(css_error).focus();
        return false;
    } else if (model == "") {
        $("#model").css(css_error).focus();
        return false;
    } else if (state == "") {
        $("#state").css(css_error).focus();
        return false;
    } else {
        return true;
    }
}


// ----------------------------
function isNumber(evt) {
    var key = window.event ? event.keyCode : event.which;
    if (event.keyCode === 8 || event.keyCode === 46) {
        return true;
    } else if ( key < 48 || key > 57 ) {
        return false;
    } else {
        return true;
    }
}