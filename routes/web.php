<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Auth::routes();
// Event::listen('illuminate.query', function ($query) {
//     var_dump($query);
// });

// Route::get('/', function () {
//     return view('welcome');
// });

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
 */
Route::get('test_api', function () {
    return view('api/test_api');
});
Route::post('/api', 'ApiController@ApiRequest');

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
 */

Route::get('/', 'AdminController@Login');
Route::get('/login', 'AdminController@Login')->name('login');
Route::post('/postlogin', 'AdminController@PostLogin');
Route::get('/logout', 'AdminController@Logout')->name('logout');

Route::get('configuration', 'AdminController@Index')->name('configuration');
Route::post('configuration/update', 'AdminController@Config')->name('configuration.update');


Route::get('/changepassword/{refkey}', 'UserController@ChangePassword')->name('changepassword');
Route::post('/postchangepassword', 'UserController@PostChangePassword')->name('post_changepassword');

Route::post('/user/CheckUniqueEmail', 'UserController@CheckUniqueEmail')->name('CheckUniqueEmail');


Route::get('dashboard', 'AdminController@Dashboard')->name('dashboard');

Route::get('user', 'UserController@Index')->name('user');
Route::get('/user/delete/{id}', 'UserController@Delete')->name('user.delete');





/*
|--------------------------------------------------------------------------
| Admin Vehicle Routes
|--------------------------------------------------------------------------
 */

Route::get('vehicle/', 'VehicleController@Index')->name('vehicle');
Route::get('vehicle/create', 'VehicleController@Create')->name('vehicle.create');
Route::post('vehicle/store', 'VehicleController@Store')->name('vehicle.store');
Route::get('vehicle/{id}/edit', 'VehicleController@Edit')->name('vehicle.edit');
Route::post('vehicle/update', 'VehicleController@Update')->name('vehicle.update');
Route::get('vehicle/delete/{id}', 'VehicleController@Destroy')->name('vehicle.delete');


/*
|--------------------------------------------------------------------------
| Admin Vehicle Category Routes
|--------------------------------------------------------------------------
 */
Route::get('category/type/{type}', 'CategoryController@Index')->name('category');
Route::get('category/create', 'CategoryController@Create')->name('category.create');
Route::post('category/store', 'CategoryController@Store')->name('category.store');
Route::get('category/{id}/edit', 'CategoryController@Edit')->name('category.edit');
Route::post('category/update', 'CategoryController@Update')->name('category.update');
Route::get('category/delete/{id}', 'CategoryController@Destroy')->name('category.delete');

/*
|--------------------------------------------------------------------------
| Admin Vehicle Brands Routes
|--------------------------------------------------------------------------
 */

Route::get('brand', 'BrandController@Index')->name('brand');
Route::get('brand/create', 'BrandController@Create')->name('brand.create');
Route::post('brand/store', 'BrandController@Store')->name('brand.store');
Route::get('brand/{id}/edit', 'BrandController@Edit')->name('brand.edit');
Route::post('brand/update', 'BrandController@Update')->name('brand.update');
Route::get('brand/delete/{id}', 'BrandController@Destroy')->name('brand.delete');

/*
|--------------------------------------------------------------------------
| Vehicle Model Routes
|--------------------------------------------------------------------------
 */

Route::get('model/{brandId}', 'ModelController@Index')->name('model');
Route::get('/model/delete/{id}', 'ModelController@Delete')->name('model.delete');
Route::get('model/create/{id}', 'ModelController@Create')->name('model.create');
Route::post('model/store', 'ModelController@Store')->name('model.store');
Route::get('model/{id}/edit', 'ModelController@Edit')->name('model.edit');
Route::post('model/update', 'ModelController@Update')->name('model.update');
Route::get('/model/delete/{id}', 'ModelController@Destroy')->name('model.delete');

/*
|--------------------------------------------------------------------------
| Admin Notification Routes
|--------------------------------------------------------------------------
 */

Route::get('notification', 'NotificationController@Index')->name('notification');
Route::post('notification/store', 'NotificationController@Store')->name('notification.store');
Route::get('/push', 'Push@Push');

/*
|--------------------------------------------------------------------------
| Help Text Routes
|--------------------------------------------------------------------------
 */

Route::get('help', 'AdminController@GetHelpText')->name('help');
Route::post('help/store', 'AdminController@SaveHelpText')->name('help.store');

Route::get('/test', 'NotificationController@test');


// --------------------------
/*
|--------------------------------------------------------------------------
| Admin Vehicle Brands Routes
|--------------------------------------------------------------------------
 */

Route::get('country', 'CountryController@Index')->name('country');
Route::get('city/create', 'CountryController@Create')->name('city.create');
Route::post('city/store', 'CountryController@StoreCity')->name('city.store');
Route::post('country/store', 'CountryController@Store')->name('country.store');
Route::get('country/get_city/{id}', 'CountryController@GetCity')->name('country.get_city');
Route::post('country/activate_city', 'CountryController@ActivateCity')->name('country.activate_city');


Route::get('country/{id}/edit', 'CountryController@Edit')->name('country.edit');
Route::post('country/update', 'CountryController@Update')->name('country.update');
Route::get('country/delete/{id}', 'CountryController@Destroy')->name('country.delete');