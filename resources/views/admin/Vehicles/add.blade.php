@extends('layouts.admin_template')
@section('title', 'Add Vehicle') 
@section('main_container')

<style>
    .image-preview{
        object-fit: contain;
        border: 1px solid #ccc;
    }
</style>

<div class="container-fluid">

    <!-- Title -->
    <div class="row heading-bg">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h5 class="txt-dark">Add Vehicle</h5>
        </div>
        <!-- Breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <a href="{{ route('vehicle') }}" class="btn btn-default pull-right">Go Back</a>
        </div>
        <!-- /Breadcrumb -->
    </div>
    <!-- /Title -->

    <!-- Row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        
                        <div class="alert alert-danger print-error-msg" style="display:none">
                            <ul></ul>
                        </div>
                        
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-wrap">
                                   {{ Form::open(['url' => route('vehicle.store'), 'role'=>'form', "enctype" => "multipart/form-data",'onsubmit'=>'return validateAddEditVehicle(); ']) }}
                                        <div class="form-body">
                                            
                                            <div class="row">

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="VehicleType">VEHICLE TYPE NAME </label>
                                                        <input type="text" id="VehicleType" pattern=".*\S+.*" name="VehicleType" class="form-control" placeholder="" required="" value="{{ old('VehicleType') }}">
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="Seating">SEATING CAPACITY </label>
                                                        <input type="text" id="Seating" name="Seating" class="form-control" placeholder="" required="" onkeypress="return isNumber(event)" value="{{ old('Seating') }}">
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="MinFare">MINIMUM FARE ($) </label>
                                                        <input type="text" id="MinFare" name="MinFare" class="form-control" required="" onkeypress="return isNumber(event)" placeholder="" value="{{ old('MinFare') }}">
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="BaseFare">BASE FARE ($) </label>
                                                        <input type="text" id="BaseFare" name="BaseFare" class="form-control" required="" onkeypress="return isNumber(event)" placeholder="" value="{{ old('BaseFare') }}">
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="PricePerMinute">PRICE PER MINUTE ($) </label>
                                                        <input type="text" id="PricePerMinute" name="PricePerMinute" class="form-control" required="" onkeypress="return isNumber(event)" placeholder="" value="{{ old('PricePerMinute') }}">
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="WaitingCharge">WAITING CHARGE PER MINUTE ($)</label>
                                                        <input type="text" id="WaitingCharge" name="WaitingCharge" class="form-control" required="" onkeypress="return isNumber(event)" placeholder="" value="{{ old('WaitingCharge') }}">
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="PricePerMile">PRICE PER/MILE ($) </label>
                                                        <input type="text" id="PricePerMile" name="PricePerMile" class="form-control" required="" onkeypress="return isNumber(event)" placeholder="" value="{{ old('PricePerMile') }}">
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="CancellationFee">CANCELATION FEE ($)</label>
                                                        <input type="text" id="CancellationFee" name="CancellationFee" class="form-control" required="" onkeypress="return isNumber(event)" placeholder="" value="{{ old('CancellationFee') }}">
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="City">CITY</label>
                                                        <select class="form-control" name="City" id="City" >
                                                        @if(!empty($cityArr))
                                                                <option value=""> SELECT CITY  </option>
                                                                @foreach($cityArr as $key => $value)
                                                                    <option value="{{ $value->CityId }}" > {{ $value->CityName }}</option>
                                                                @endforeach
                                                        @endif
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="Description">VEHICLE TYPE DESCRIPTION</label>
                                                        <textarea class="form-control" rows="4" required="" id="Description" name="Description"></textarea>
                                                    </div>
                                                </div>


                                            </div>

                                            <div class="seprator-block"></div>
                                            <h6 class="txt-dark capitalize-font"><i class="zmdi zmdi-info-outline mr-10"></i> Images </h6>
                                            <hr class="light-grey-hr"/>
                                            <div class="row" id="image_tier">
                                    
                                               
                                                <div class="col-md-12 row mt-10">
                                                    <div class="form-group">
                                                        <div class="col-md-4">
                                                            <label class="control-label mb-10"> VEHICLE ON IMAGE</label>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <div class="fileupload btn btn-info btn-anim">
                                                                <i class="fa fa-upload cursor-pointer"></i><span class="btn-text">Upload image</span>
                                                                <input type="file" class="upload" required="" name="OnImage" id="OnImage" onchange="uploadImage(this);">
                                                            </div>
                                                            <div>
                                                                <img src="" id="img" style="display: none;" height="200px" width="200px" class="image-preview mt-5">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-12 row mt-10">
                                                    <div class="form-group">
                                                        <div class="col-md-4">
                                                            <label class="control-label mb-10"> VEHICLE OFF IMAGE</label>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <div class="fileupload btn btn-info btn-anim">
                                                                <i class="fa fa-upload cursor-pointer"></i><span class="btn-text">Upload image</span>
                                                                <input type="file" class="upload" required="" name="OffImage" id="OffImage" onchange="uploadImage1(this);">
                                                            </div>
                                                            <div>
                                                                <img src="" id="img1" style="display: none;" height="200px" width="200px" class="image-preview mt-5">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-12 row mt-10">
                                                    <div class="form-group">
                                                        <div class="col-md-4">
                                                            <label class="control-label mb-10"> MAP ICON</label>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <div class="fileupload btn btn-info btn-anim">
                                                                <i class="fa fa-upload cursor-pointer"></i><span class="btn-text">Upload image</span>
                                                                <input type="file" class="upload" required="" name="MapIcon" id="MapIcon" onchange="uploadImage2(this);">
                                                            </div>
                                                            <div>
                                                                <img src="" id="img2" style="display: none;" height="200px" width="200px" class=" image-preview mt-5">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>



                                        </div>
                                    
                                        <div class="form-actions mt-10">
                                            <button type="button" class="btn btn-success mr-10"> Save</button>
                                        </div>
                                    {{ Form::close() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>	
        </div>
    </div>
    <!-- /Row -->
</div>


<!-- /page content -->
@endsection

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $(".select2").select2();
 
        });

    </script>

    <script type="text/javascript">
        
        // Save order
        $('.btn-success').click(function(){

            var VehicleType     = $('#VehicleType').val();
            var Seating         = $('#Seating').val();
            var BaseFare        = $('#BaseFare').val();
            var MinFare         = $('#MinFare').val();
            var WaitingCharge   = $('#WaitingCharge').val();
            var CancellationFee = $('#CancellationFee').val();
            var PricePerMinute  = $('#PricePerMinute').val();
            var PricePerMile    = $('#PricePerMile').val();
            var Description     = $('#Description').val();
            var City            = $('#City').val();
            var OnImage         = $('#OnImage').val();
            var OffImage        = $('#OffImage').val();
            var MapIcon         = $('#MapIcon').val();

            var OnImage         = $('#OnImage')[0].files[0];
            var OffImage        = $('#OffImage')[0].files[0];
            var MapIcon         = $('#MapIcon')[0].files[0];

            // var css_error = ({
            //     'border-color': 'red'
            // });
            // var css_original = ({
            //     'border-color': 'rgb(204, 204, 204)'
            // });

            // $("#vehiclecategory,#brands,#lineas,#model,#state").css(css_original);

            var error = '';
            $('.print-error-msg').show();
            $('.print-error-msg ul').html(error);
            if(VehicleType == ''){
                error += '<li>Vehicle Type is required</li>';
                // $("#vehiclecategory").focus();
                // $("#vehiclecategory").css(css_error);
                // return false;
            }if(Seating == ''){
                error += '<li>Seating capicity is required</li>';
                // $("#brands").focus();
                // $("#brands").css(css_error);
                // return false;
            }if(BaseFare == ''){
                error += '<li>Base Fare is required</li>';
            }if(MinFare == ''){
                error += '<li>Min Fare is required</li>';
            }if(WaitingCharge == ''){
                error += '<li>Waiting Charge is required</li>';
            }if(CancellationFee == ''){
                error += '<li>Cancellation Fee is required</li>';
            }if(PricePerMinute == ''){
                error += '<li>Price Per Minute is required</li>';
            }if(PricePerMile == ''){
                error += '<li>Price Per Mile is required</li>';
            }if(Description == ''){
                error += '<li>Description is required</li>';
            }if(City == ''){
                error += '<li>Select atleat one city</li>';
                // $("#files").focus();
                // swal('Plase upload an image');
                // return false;
            }if(OnImage == undefined){
                error += '<li>On image is required</li>';
                // $("#files").focus();
                // swal('Plase upload an image');
                // return false;
            }if(OffImage == undefined){
                error += '<li>Off image is required</li>';
                // $("#files").focus();
                // swal('Plase upload an image');
                // return false;
            }if(MapIcon == undefined){
                error += '<li>Map icon is required</li>';
                // $("#files").focus();
                // swal('Plase upload an image');
                // return false;
            }

            if(error!= ''){
                $('.print-error-msg').show();
                $('.print-error-msg ul').html(error);
                $(window).scrollTop($('.print-error-msg').offset().top);
                return false;
            }else{
                $('.print-error-msg').hide();
            }


            $('.btn-success').attr('disabled','disabled');
            $('.loader_div').show();

            var form_data = new FormData(); 
            form_data.append('VehicleType',VehicleType);
            form_data.append('Seating',Seating);
            form_data.append('BaseFare',BaseFare);
            form_data.append('MinFare',MinFare);
            form_data.append('WaitingCharge',WaitingCharge);
            form_data.append('CancellationFee',CancellationFee);
            form_data.append('PricePerMinute',PricePerMinute);
            form_data.append('PricePerMile',PricePerMile);
            form_data.append('Description',Description);
            form_data.append('City',City);
            form_data.append('OnImage',OnImage);
            form_data.append('OffImage',OffImage);
            form_data.append('MapIcon',MapIcon);

            var html = '';

            var url= "../vehicle/store";
            $.ajax({
                type: "POST",
                url: url,
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                success: function (response) { 

                    window.location.href = '../vehicle';
                }
            });
        });

    </script>
@endpush