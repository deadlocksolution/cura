@extends('layouts.admin_template')
@section('title', 'Update Customer') 
@section('main_container')

<div class="container-fluid">

    <!-- Title -->
    <div class="row heading-bg">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h5 class="txt-dark">Update Customer</h5>
        </div>
        <!-- Breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <a href="{{ route('customer.index') }}" class="btn btn-default pull-right">Go Back</a>
        </div>
        <!-- /Breadcrumb -->
    </div>
    <!-- /Title -->

    <!-- Row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        
                        @if (session()->has('flash_notification.message'))
                            <div class="alert alert-{{ session('flash_notification.level') }}" style="clear: both;">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {!! session('flash_notification.message') !!}
                            </div>
                        @endif
                        
                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                <div class="form-wrap">
                                    {{ Form::open(['url' => route('customer.update'), 'role'=>'form', "enctype" => "multipart/form-data", "onsubmit" => "return validateEditUser();"]) }}
                                        
                                        <input type="hidden" name="id" value="{{ $userDataArr->id }}">
                                        <div class="form-body">
                                            <h6 class="txt-dark capitalize-font"><i class="zmdi zmdi-account mr-10"></i>Person's Info</h6>
                                            <hr class="light-grey-hr"/>
                                            
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="first_name">First Name *</label>
                                                        <input type="text" id="first_name" name="first_name" class="form-control" required="" placeholder="Enter First Name" value="{{ $userDataArr->first_name }}">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="last_name">Last Name*</label>
                                                        <input type="text" id="last_name" name="last_name" class="form-control" required="" placeholder="Enter Last Name" value="{{ $userDataArr->last_name }}">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="phone">Phone *</label>
                                                        <input type="text" id="phone" name="phone" class="form-control" disabled="" placeholder="Enter Phone Number" value="{{ $userDataArr->email }}">
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="email">Email</label>
                                                        <input type="email" id="email" name="email" class="form-control" placeholder="Enter Email Id" value="{{ $userDataArr->profile->email }}">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="password">Password</label>
                                                        <input type="password" id="password" name="password" class="form-control" placeholder="Enter Password">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="cpassword">Confirm Password</label>
                                                        <input type="password" id="cpassword" class="form-control" placeholder="Enter Confirm Password">
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="company_name">Company Name</label>
                                                        <input type="text" id="company_name" name="company_name" class="form-control" placeholder="Enter Company Name" value="{{ $userDataArr->profile->company_name }}">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="gst_number">GST Number</label>
                                                        <input type="text" id="gst_number" name="gst_number" class="form-control" placeholder="Enter GST Number" value="{{ $userDataArr->profile->gst_number }}">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="profile_pic">Profile Picture</label>
                                                        <input type="file" id="profile_pic" name="profile_pic" >
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="is_user_verify">Is User Verify? *</label>
                                                        <select class="form-control" name="is_user_verify" id="is_user_verify">
                                                            <option value="1" <?php echo (isset($userDataArr->is_user_verify) && $userDataArr->is_user_verify == 1) ? 'selected="selected"' : "" ?>>Verified</option>
                                                            <option value="0" <?php echo (isset($userDataArr->is_user_verify) && $userDataArr->is_user_verify == 0) ? 'selected="selected"' : "" ?>>Not Verified</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="status">Status</label>
                                                        <select class="form-control" name="status" id="status">
                                                            <option value="1" <?php echo (isset($userDataArr->profile->status) && $userDataArr->profile->status == 1) ? 'selected="selected"' : "" ?>>Activate</option>
                                                            <option value="0" <?php echo (isset($userDataArr->profile->status) && $userDataArr->profile->status == 0) ? 'selected="selected"' : "" ?>>Deactivate</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        @if(isset($userDataArr->profile->profile_pic) && $userDataArr->profile->profile_pic != "")
                                                            <img src="{{ asset("public/images/users") . "/" . $userDataArr->profile->profile_pic }}" width="150px" height="150px">
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <!-- /Row -->

                                            <div class="seprator-block"></div>

                                            <h6 class="txt-dark capitalize-font"><i class="zmdi zmdi-account-box mr-10"></i>address</h6>
                                            <hr class="light-grey-hr"/>
                                            
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="address">Address *</label>
                                                        <textarea id="address" name="address" class="form-control" placeholder="Enter Address">{{ $userDataArr->profile->address }}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="country">Country *</label>
                                                        <select class="form-control select2" required="" name="country" id="country" onchange="changeCountry(this.value);">
                                                            <option value="">Select Country</option>
                                                            @foreach($country_data AS $country_value)
                                                            <option value="{{ $country_value['id'] }}" <?php echo (isset($userDataArr->profile->country) && $userDataArr->profile->country == $country_value['id']) ? 'selected="selected"' : ""; ?>>{{ $country_value['name'] }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="state">State *</label>
                                                        <select class="form-control select2" required="" name="state" id="state" onchange="changeState(this.value);">
                                                            <option value="">Select State</option>
                                                            @foreach($state_data AS $state_value)
                                                            <option value="{{ $state_value['id'] }}" <?php echo (isset($userDataArr->profile->state) && $userDataArr->profile->state == $state_value['id']) ? 'selected="selected"' : ""; ?>>{{ $state_value['name'] }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="city">City *</label>
                                                        <select class="form-control select2" required="" name="city" id="city">
                                                            <option value="">Select City</option>
                                                            @foreach($city_data AS $city_value)
                                                            <option value="{{ $city_value['id'] }}" <?php echo (isset($userDataArr->profile->city) && $userDataArr->profile->city == $city_value['id']) ? 'selected="selected"' : ""; ?>>{{ $city_value['name'] }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="pincode">Pincode</label>
                                                        <input type="text" id="pincode" name="pincode" class="form-control" placeholder="Enter Pincode" value="{{ $userDataArr->profile->pincode }}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-actions mt-10">
                                            <button type="submit" class="btn btn-success  mr-10"> Save</button>
                                            <span class="color-red" id="password_mismatch_error" style="display: none;">Your password and confirmation password do not match.</span>
                                            <span class="color-red" id="password_length_error" style="display: none;">Passwords must be at least 5 characters in length.</span>
                                        </div>
                                    {{ Form::close() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>	
        </div>
    </div>
    <!-- /Row -->
</div>

<!-- /page content -->
@endsection

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $(".select2").select2();
        });
    </script>
@endpush