@extends('layouts.admin_template')
@section('title', 'Customer List') 
@section('main_container')

<div class="container-fluid">

    <!-- Title -->
    <div class="row heading-bg">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h5 class="txt-dark">Customer List</h5>
        </div>
        <!-- Breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <a href="{{ route('customer.create') }}" class="btn btn-primary pull-right">Add Customer</a>
        </div>
        <!-- /Breadcrumb -->
    </div>
    <!-- /Title -->

    <!-- Row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="table-wrap">
                            
                            @if (session()->has('flash_notification.message'))
                                <div class="alert alert-{{ session('flash_notification.level') }}" style="clear: both;">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    {!! session('flash_notification.message') !!}
                                </div>
                            @endif
                            
                            <div class="table-responsive">
                                
                                <table id="data_table" class="table table-hover display  pb-30" >
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Phone</th>
                                            <th>Email</th>
                                            <th>Country</th>
                                            <th>State</th>
                                            <th>City</th>
                                            <th>Company Name</th>
                                            <th>Created at</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Phone</th>
                                            <th>Email</th>
                                            <th>Country</th>
                                            <th>State</th>
                                            <th>City</th>
                                            <th>Company Name</th>
                                            <th>Created at</th>
                                            <th>Action</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        
                                        @if(isset($usersDataArr) && count($usersDataArr) > 0)
                                            @foreach($usersDataArr as $user_key => $user_value)

                                                <tr data-row-id="{{ $user_value->id }}" id="record_{{ $user_value->id }}">
                                                    <td>{{ ++$user_key }}</td>
                                                    <td>{{ $user_value->first_name }} {{ $user_value->last_name }}</td>
                                                    <td>{{ $user_value->phone }}</td>
                                                    <td>{{ (isset($user_value->profile->email) && $user_value->profile->email != "") ? $user_value->profile->email : "" }}</td>
                                                    <td>{{ (isset($user_value->profile->country_details->name) && $user_value->profile->country_details->name != "") ? $user_value->profile->country_details->name : "" }}</td>
                                                    <td>{{ (isset($user_value->profile->state_details->name) && $user_value->profile->state_details->name != "") ? $user_value->profile->state_details->name : "" }}</td>
                                                    <td>{{ (isset($user_value->profile->city_details->name) && $user_value->profile->city_details->name != "") ? $user_value->profile->city_details->name : "" }}</td>
                                                    <td>{{ (isset($user_value->profile->company_name) && $user_value->profile->company_name != "") ? $user_value->profile->company_name : "" }}</td>
                                                    <td>{{ $user_value->created_at }}</td>
                                                    <td>
                                                        <a href="{{ route('customer.edit', array($user_value->id)) }}">
                                                            <i class="fa fa-pencil-square-o fa-2x"></i> 
                                                        </a>&nbsp;&nbsp;   
                                                    
                                                        <a href="javascript:void(0);" onclick="deletePopup('customer/delete/', {{ $user_value->id }})">
                                                            <i class="fa fa-trash-o fa-2x"></i> 
                                                        </a>
                                                    </td>
                                                </tr>
                                                
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>	
        </div>
    </div>
    <!-- /Row -->
</div>

<!-- /page content -->
@endsection

@push('scripts')
    <script type="text/javascript"> 
        $(document).ready(function() {
            $('#data_table').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "aoColumnDefs": [{ "bSortable": false, "aTargets": [ 9 ] }],
                "aaSorting": [[8,'DESC']]
            });  
        });  
    </script>
@endpush