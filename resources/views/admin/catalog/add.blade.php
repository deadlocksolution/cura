@extends('layouts.admin_template')
@section('title', 'Add Catalog') 
@section('main_container')

<div class="container-fluid">

    <!-- Title -->
    <div class="row heading-bg">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h5 class="txt-dark">Add Catalog</h5>
        </div>
        <!-- Breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <a href="{{ route('catalog.index') }}" class="btn btn-default pull-right">Go Back</a>
        </div>
        <!-- /Breadcrumb -->
    </div>
    <!-- /Title -->

    <!-- Row -->
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <div class="panel panel-default card-view">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        
                        @if (session()->has('flash_notification.message'))
                            <div class="alert alert-{{ session('flash_notification.level') }}" style="clear: both;">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {!! session('flash_notification.message') !!}
                            </div>
                        @endif
                        
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-wrap">
                                    {{ Form::open(['url' => route('catalog.store'), 'role'=>'form', "enctype" => "multipart/form-data"]) }}
                                        <div class="form-body">
                                            
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="category_id">Category *</label>
                                                        <select class="form-control" name="category_id" id="category_id" required="">
                                                            <option value="">Select Category</option>
                                                            @foreach($categoryArr AS $category_value)
                                                                <option value="{{ $category_value->id }}">{{ $category_value->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="file">Catalog File *</label>
                                                        <input type="file" id="file" name="file" required="">
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="status">Status</label>
                                                        <select class="form-control" name="status" id="status">
                                                            <option value="1">Enable</option>
                                                            <option value="0">Disable</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    
                                        <div class="form-actions mt-10">
                                            <button type="submit" class="btn btn-success mr-10"> Save</button>
                                        </div>
                                    {{ Form::close() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>	
        </div>
    </div>
    <!-- /Row -->
</div>

<!-- /page content -->
@endsection