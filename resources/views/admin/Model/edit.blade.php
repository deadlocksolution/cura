@extends('layouts.admin_template')
@section('title', 'Update Brands') 
@section('main_container')

<div class="container-fluid">

    <!-- Title -->
    <div class="row heading-bg">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h5 class="txt-dark">UPDATE MODEL</h5>
        </div>
        <!-- Breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <a href="{{ route('model',array('auto')) }}" class="btn btn-default pull-right">Go Back</a>
        </div>
        <!-- /Breadcrumb -->
    </div>
    <!-- /Title -->

    <!-- Row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        
                        @if (session()->has('flash_notification.message'))
                            <div class="alert alert-{{ session('flash_notification.level') }}" style="clear: both;">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {!! session('flash_notification.message') !!}
                            </div>
                        @endif
                        
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-wrap">
                                    {{ Form::open(['route'=>['model.update'], 'enctype' => 'multipart/form-data', 'method'=>'post'])}} 
                                        <div class="form-body">
                                            <input type="hidden" name="id" value="{{ $ModelData->ModelId }}">
                                            <div class="row">
                                                    
                                                
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="ModelName">Model Name *</label>
                                                        <input type="text" id="ModelName" pattern=".*\S+.*" name="ModelName" class="form-control" required="" placeholder="" value="{{$ModelData->ModelName}}">
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    
                                        <div class="form-actions mt-10">
                                            <button type="submit" class="btn btn-success mr-10"> Save</button>
                                        </div>
                                    {{ Form::close() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>	
        </div>
    </div>
    <!-- /Row -->
</div>

<!-- /page content -->
@endsection
@push('scripts')
<script type="text/javascript">
$(document).ready(function() {
    $(".select2").select2();
});

</script>
@endpush