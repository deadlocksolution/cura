@extends('layouts.admin_template')
@section('title', 'Dashboard')

@push('stylesheets')

@endpush

@section('main_container')

<div class="container-fluid pt-25">
    <!-- Row -->
    <div class="row">

    	 {{ Session::get('AdminId') }}

    	 {{ Session::get('Email') }}

    	 {{ Session::get('Type') }}

    </div>
    <!-- /Row -->
</div>

<!-- /page content -->
@endsection