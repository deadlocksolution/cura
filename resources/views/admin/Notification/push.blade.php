@extends('layouts.admin_template')
@section('title', 'PUSH')
@section('main_container')

<style type="text/css">

.select2-results__options[aria-multiselectable="true"] li {
    padding-left: 30px;
    position: relative
}

.select2-results__options[aria-multiselectable="true"] li:before {
    position: absolute;
    left: 8px;
    opacity: .6;
    top: 6px;
    font-family: "FontAwesome";
    content: "\f0c8";
}

.select2-results__options[aria-multiselectable="true"] li[aria-selected="true"]:before {
    content: "\f14a";
}

.select2-container--default .select2-selection--single .select2-selection__rendered, .select2-container--default .select2-results__option[aria-selected="true"] {
    padding-left: 30px;
}
.select2-container--default .select2-selection--multiple .select2-selection__choice__remove{
    padding: 5px;
}
.select2-container .select2-selection--multiple .select2-selection__rendered{
    line-height: 38px;
}
.select2-container--default .select2-selection--multiple{
    border: solid #ccc 1px !important;
    border-radius: 0 !important;
}
</style>

<div class="container-fluid">

    <!-- Title -->
    <div class="row heading-bg">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h5 class="txt-dark">SEND PUSH NOTIFICATION</h5>
        </div>
        <!-- Breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <!-- <a href="{{ route('user') }}" class="btn btn-default pull-right">Go Back</a> -->
        </div>
        <!-- /Breadcrumb -->
    </div>
    <!-- /Title -->

    <!-- Row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">

                        @if (session()->has('flash_notification.message'))
                            <div class="alert alert-{{ session('flash_notification.level') }}" style="clear: both;">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {!! session('flash_notification.message') !!}
                            </div>
                        @endif

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-wrap">
                                    {{ Form::open(['url' => route('notification.store'), 'role'=>'form', "enctype" => "multipart/form-data"]) }}
                                        <div class="form-body">

                                            <div class="row">

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="NotificationText"> NOTIFICATION TEXT *</label>
                                                        <input type="text" id="NotificationText" pattern=".*\S+.*" name="NotificationText" class="form-control" required="" placeholder="" value="{{ old('NotificationText') }}">
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="row">

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="UserId"> SELECT DRIVERS *</label>
                                                        <select class="form-control select2" multiple="multiple" name="UserId[]" id="UserId">
                                                            @if(!empty($UserData))
                                                                <option value=""> SELECT DRIVER  </option>
                                                                @foreach($UserData as $key => $value)
                                                                    <option value="{{ $value->UserId }}"> {{ $value->FirstName }}  {{ $value->LastName }}</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>

                                        <div class="form-actions mt-10">
                                            <button type="submit" class="btn btn-success mr-10"> Send Push</button>
                                        </div>
                                    {{ Form::close() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Row -->
</div>

<!-- /page content -->
@endsection

@push('scripts')
  
<script type="text/javascript">
    $(document).ready(function() {
       $(".select2").select2();

       $('.select2[multiple]').select2({
    width: '100%',
    closeOnSelect: false
})
    });
</script>
  
@endpush
