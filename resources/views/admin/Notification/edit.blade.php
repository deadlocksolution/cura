@extends('layouts.admin_template')
@section('title', 'Update Notification')
@section('main_container')

<div class="container-fluid">

    <!-- Title -->
    <div class="row heading-bg">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h5 class="txt-dark">Actualizar inserción por ubicación</h5>
        </div>
        <!-- Breadcrumb -->
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <a href="{{ route('push-location') }}" class="btn btn-default pull-right">Go Back</a>
        </div>
        <!-- /Breadcrumb -->
    </div>
    <!-- /Title -->

    <!-- Row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default card-view">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">

                        @if (session()->has('flash_notification.message'))
                            <div class="alert alert-{{ session('flash_notification.level') }}" style="clear: both;">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {!! session('flash_notification.message') !!}
                            </div>
                        @endif

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-wrap">
                                    {{ Form::open(['route'=>['notification.update'], 'enctype' => 'multipart/form-data', 'method'=>'post'])}}
                                        <div class="form-body">
                                            <input type="hidden" name="id" value="{{ $NotificationData->NotificationId }}">
                                            <div class="row">

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="NotificationText">Mensaje *</label>
                                                        <input type="text" id="NotificationText" pattern=".*\S+.*" name="NotificationText" class="form-control" required="" placeholder="Enter Mensaje" value="{{ $NotificationData->NotificationText }}">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="Range">Distancia *</label>
                                                        <select class="form-control" name="Range" id="Range">
                                                            @for ($i = '1'; $i <= '50'; $i++)
                                                                <option value="{{ $i }}" <?php echo (isset($NotificationData->Range) && $NotificationData->Range == 1) ? 'selected="selected"' : ""; ?>>{{ $i }}</option>
                                                            @endfor
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="Latitude">Latitud</label>
                                                        <input type="text" id="Latitude" pattern=".*\S+.*" name="Latitude" class="form-control" required="" placeholder="Enter Latitud" value="{{ $NotificationData->Latitude }}">
                                                    </div>
                                                </div>


                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label mb-10" for="Longitude">Longitud</label>
                                                        <input type="text" id="Longitude" pattern=".*\S+.*" name="Longitude" class="form-control" required="" placeholder="Enter Longitud" value="{{ $NotificationData->Longitude }}">
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="form-actions mt-10">
                                            <button type="submit" class="btn btn-success mr-10"> Save</button>
                                        </div>
                                    {{ Form::close() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Row -->
</div>

<!-- /page content -->
@endsection
@push('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $(".select2").select2();
        });
        VehicleType({{ $NotificationData->VehicleType }} );
        function VehicleType(type){

            var categoryData =<?= json_encode(explode(',', $NotificationData->VehicleCategories )); ?>;
            
            $.ajax({
                type: "GET",
                url: "../../vehicle/get_vehicle_categories/"+type,
                data: {
                    // 'type' : type
                },
                success: function (res) {
                    var obj = JSON.parse(res);

                    $('#vehiclecategories').prop('disabled', false).html('').append('<option value="">Seleccione uno </option>');
                    for (var i = 0; i < obj.length; i++) {
                        var selected = '';
                        if (jQuery.inArray(String(obj[i].VehicleCategoryId), categoryData) != '-1') {
                            selected = 'selected';
                        } 

                        $('#vehiclecategories').append('<option value=' + obj[i].VehicleCategoryId + ' ' + selected + '>' + obj[i].VehicleName + '</option>');
                    }
                }
            });
        }


    </script>
@endpush