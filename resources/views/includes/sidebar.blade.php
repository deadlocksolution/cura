
<div class="fixed-sidebar-left">
    <ul class="nav navbar-nav side-nav nicescroll-bar">

       
        <li>
            <a href="{{ route('vehicle') }}">
                <i class="zmdi zmdi-car-taxi mr-20"></i>
                <span class="right-nav-text">VEHICLE TYPES</span>
            </a>
        </li>

        <li>
            <a href="{{ route('country') }}">
                <i class="zmdi zmdi-accounts mr-20"></i>
                <span class="right-nav-text">COUNTRY AND CITY</span>
            </a>
        </li>

        <li>
            <a href="{{ route('brand') }}">
                <i class="zmdi zmdi-accounts mr-20"></i>
                <span class="right-nav-text">VEHICLE MODELS</span>
            </a>
        </li>

        <li>
            <a href="{{ route('notification') }}">
                <i class="zmdi zmdi-accounts mr-20"></i>
                <span class="right-nav-text">PUSH NOTIFICATION</span>
            </a>
        </li>

        <li>
            <a href="{{ route('help') }}">
                <i class="zmdi zmdi-accounts mr-20"></i>
                <span class="right-nav-text">HELP TEXT</span>
            </a>
        </li>
        <!--  -->

        <!-- 
        
       
        <li>
            <a href="{{ route('configuration') }}">
                <i class="zmdi zmdi-accounts mr-20"></i>
                <span class="right-nav-text">Configuration</span>
            </a>
        </li>
 -->
    </ul>
</div>
<!-- /Left Sidebar Menu